feedparser==5.2.1
numpy==1.15.4
pandas==0.23.4
pathlib==1.0.1
python-dateutil==2.7.5
pytz==2018.7
six==1.12.0
tqdm==4.28.1
